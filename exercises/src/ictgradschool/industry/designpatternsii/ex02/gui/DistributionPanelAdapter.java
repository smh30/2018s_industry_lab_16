package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {
 
    private Course course;
    private DistributionPanel panel;
    
    public DistributionPanelAdapter(DistributionPanel panel){
        this.panel = panel;
    }
    
	@Override
	public void courseHasChanged(Course course) {
	    // make the distribution panel paint() itself
        //how to do this without having a Graphics g to hand it??????
        panel.repaint();
	
	}
	
	
	
	
	
	
	
	
}
