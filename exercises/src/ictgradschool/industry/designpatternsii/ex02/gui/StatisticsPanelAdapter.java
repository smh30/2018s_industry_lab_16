package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {
	
	private Course course;
	private StatisticsPanel panel;
	
	public StatisticsPanelAdapter(StatisticsPanel panel){
	    this.panel = panel;
    }
	
	@Override
	public void courseHasChanged(Course course) {
	    panel.repaint();
	}
	
	
	
}
