package ictgradschool.industry.designpatternsii.ex02.model;


import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.Iterator;

public class CourseAdapter extends AbstractTableModel implements CourseListener, TableModelListener {
    
    final int STUD_ID = 0;
    final int SURNAME = 1;
    final int FIRSTNAME = 2;
    final int EXAM = 3;
    final int TEST = 4;
    final int ASSI = 5;
    final int OVERALL = 6;
    
    
    private Course course;
    
    public CourseAdapter(Course course) {
        this.course = course;
        //course.addCourseListener(this);
        
    }
    
    @Override
    public void courseHasChanged(Course course) {
        System.out.println("course adapter sees change");
        System.out.println(course.getResultAt(0));
        
        fireTableDataChanged();
        
       
        
    }
    
    
    @Override
    public int getRowCount() {
        return course.size();
    }
    
    @Override
    public int getColumnCount() {
        // not sure how best to extract that, so just hard coding it for now
        return 7;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        // likewise hardcoding this
        switch (columnIndex) {
            case STUD_ID:
                return "Student ID";
            case SURNAME:
                return "Surname";
            case FIRSTNAME:
                return "Forename";
            case EXAM:
                return "Exame";
            case TEST:
                return "Test";
            case ASSI:
                return "Assignment";
            case OVERALL:
                return "Overall";
        }
        return "";
    }
    

 //brain wasn't working, borrowed this method from Darcy's code
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        switch (columnIndex){
            case STUD_ID:
                return course.getResultAt(rowIndex)._studentID;
            case SURNAME:
                return course.getResultAt(rowIndex)._studentSurname;
            case FIRSTNAME:
                return course.getResultAt(rowIndex)._studentForename;
            case EXAM:
                return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case TEST:
                return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
            case ASSI:
                return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case OVERALL:
                return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);
        }
        
        return null;
        
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    
    
    }
    
    @Override
    public void addTableModelListener(TableModelListener l) {
    
    }
    
    @Override
    public void removeTableModelListener(TableModelListener l) {
    
    }
    
    @Override
    public void tableChanged(TableModelEvent e) {
    ;
    }
}